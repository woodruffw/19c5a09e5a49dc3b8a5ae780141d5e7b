pairs = gets.chomp.split.map { |p| p.split(/d/i).map(&:to_i) }
pairs.each { |s, t| puts "You rolled %s" % [*1..s].sample(t).reduce(:+) }
